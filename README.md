# Daily Log

A small OS X script that asks you every half hour to write down what you have been up to.

## Dependencies

Install <https://github.com/alloy/terminal-notifier>. If you are using Homebrew run `brew install terminal-notifier`.

## Installation

- Clone the repository;
- Install the LaunchAgent with `bin/install`.
